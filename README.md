# Project Manager MERN App

## Project description:
    Free Project Manager
    A free and open source Project Manager built using MERN stack.
    You can create as many projects as you want. Have them private or public for everyone to see or not. Each project has it's own board which only the author and project members can edit. Each card when clicked on brings up a comments window.
    Collaborate with as many people as needed by adding them to your project using their email.

## Project overview:

    Server API : Express + MongoDB
    Client : React

    1. Local Auth with JWT (Login/Register/Logout)
    2. Public Projects - Visible by all users (editable only by creator/group members)
    3. Private projects - Visible by its creator and group memebers only.
    4. Project Details Page - Basic information + Project Tasks/Edit/Delete
    5. Project Board (Tasks) - Drag and Droppable Tabs and Cards for tasks.
    6. Profile Page - Basic user info + created/shared projects.
    7. CRUD operations on Projects and Board (tasks)
    8. Each task can be clicked on to show a window with comments from project members.
    9. From project edit page author's can add unlimited amount of members to each project.

# Installation

1. Client

```sh
cd src/client
npm install
```
2. Server

```sh
cd src/server
npm install
```
