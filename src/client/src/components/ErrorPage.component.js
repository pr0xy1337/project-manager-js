import './ErrorPage.css'
function ErrorPage() {
    return (
        <div className="row h-100">
            <div className="col-12 col-md-10 mx-auto my-auto">
                <h1 id="notFound">Error 404: Page not found</h1>
            </div>
        </div>
       
    );
};

export default ErrorPage;